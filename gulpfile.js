var gulp = require('gulp');
var jshint = require('gulp-jshint');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var minifyHtml = require('gulp-minify-html');
var plumber = require('gulp-plumber');
var less = require('gulp-less');
var autoprefixer = require('gulp-autoprefixer');
var del = require('del');
var webserver = require('gulp-webserver');

var paths = {
    build: 'build',
    scripts: ['src/app/**/*.js'],
    less: ['src/**/*.less'],
    index: ['src/index.html']
};

gulp.task('webserver', function() {
    return gulp.src('./').pipe(webserver({
        port: '8081',
        host: '0.0.0.0'
    }));
});

gulp.task('scripts', function() {
    return gulp.src(paths.scripts)
        .pipe(concat('app.js'))
        .pipe(jshint())
        .pipe(uglify())
        .pipe(gulp.dest(paths.build + '/js'))
}); 

gulp.task('less', function() {
    return gulp.src(paths.less)
        .pipe(less())
        .pipe(autoprefixer('last 2 versions'))
        .pipe(gulp.dest(paths.build))
});

gulp.task('copyIndex', function() {
    return gulp.src(paths.index)
        .pipe(minifyHtml())
        .pipe(gulp.dest(paths.build));
});

gulp.task('clean', function() {
    del(['build']);
});

gulp.task('build-dev', ['scripts', 'less', 'copyIndex']);

gulp.task('default', ['build-dev']);